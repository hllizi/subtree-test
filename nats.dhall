let NatAbst = \(Nat : Type) -> < Zero | Succ : Nat >
let NatAbstType = forall(Nat : Type) -> NatAbst Nat
let Nat = NatAbst NatAbstType
let zero = Nat.Zero
let succ = Nat.Succ 
in Nat.Succ (\(n : Type) -> )
