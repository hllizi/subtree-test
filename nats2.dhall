let NatAbst : Type = 
               forall(Nat : Type) -> 
                  forall(Zero : Nat) ->
                    forall(Succ : Nat -> Nat) 
                      -> Nat
let zero = \(Nat : Type) -> \(Zero : Nat) -> \(Succ : Nat -> Nat) -> Zero
let succ = \(natabst : NatAbst) -> \(Nat : Type) -> \(Zero : Nat) -> \(Succ : Nat -> Nat) -> Succ (natabst Nat Zero Succ)

let toNatural : forall(natabst : NatAbst) -> Natural = \(natabst : NatAbst) -> natabst Natural 0 (\(n : Natural) -> n + 1)

in toNatural (succ zero)



